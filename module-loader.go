package masp

import (
	"github.com/coreos/go-semver/semver"
	"github.com/gin-gonic/gin"
)

// Grundprinzip stammt vom https://github.com/moqmar/gouda

//modules sind alle Module
var modules = []Module{}

// Module beschreibt ein beliebiges Modul
type Module interface {
	//Info gibt den Name des Moduls, ModulID, Version, Verzeichnis zurück
	Info() (string, string, *semver.Version, string)
	//Routes gibt die Routen des Moduls zurück.
	// Über diese Funktion werden die Routen der einzelnen Module in das Basismodul geladen, wo der Webserver läuft.
	Routes(*gin.RouterGroup)
}

// RegisterModule registriert ein Modul im Basismodul
// und stellt ihm die API zur Verfügung
func RegisterModule(m Module) API {
	modules = append(modules, m)
	return API{module: m}
}
