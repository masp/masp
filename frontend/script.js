import "babel-polyfill";

import Vue from "vue";
import App from "./vue/app.vue";
import RestApi from "./rest-api";
import modules from "./modules.js";

window.api = new RestApi("/", {
  headers: { "Authorization": "Bearer " + localStorage.hash }
});

(async () => {
  window.config = {};
  let mod = [App, ...modules];
  for (let i = 0; i < mod.length; i++) {
    if (typeof mod[i].settings == "function") {
      let set = mod[i].settings();
      if (set && set.settings) for (let i = 0; i < set.settings.length; i++) {
        window.config[set.settings[i].name] = ((await api.GET("/api/base/settings/" + encodeURIComponent(set.settings[i].name)))||{}).content;
      }
    }
  }

  try {
    const r = await api.GET("/api/users/user/me");
    if (r.ok) window.user = r.content;
    else window.user = null;
  } catch (e) { window.user = null; }

  window.app = new Vue({
    el: "main",
    render: h => h(App)
  });
})();
