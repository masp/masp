package main

import (

	// Example 1.0.0
	_ "git.mo-mar.de/masp/masp/example-module"

	// User Accounts 0.1.0
	_ "git.mo-mar.de/masp/users"

	// Task List 0.1.0
	_ "git.mo-mar.de/masp/tasks"

)
