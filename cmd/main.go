package main

import "git.mo-mar.de/masp/masp"

// Haupt Start Punkt für alles
func main() {
	masp.Start()
}
