package masp

import (
	"io"
	"os"

	log "github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

// Log erstellt einen neuen Event-gebundenen Logger
// l := m.Log("Server Startup")
// l.Info("Hello %s", "World")
func (a API) Log(event string) Logger {
	name, _, _, _ := a.module.Info()
	return Logger{
		module: a.module,
		event:  event,
		requestlogger: log.WithFields(log.Fields{

			//"event":  event,
			//"module": name,
			"prefix": name + " - " + event,
			//"User": user,
		}),
	}
}

// Der MASP Logger.
// Beinhaltet alles was der Logger braucht
type Logger struct {
	// Eigenschaften
	module Module
	event  string
	//user   User
	requestlogger *log.Entry
}

// Logged eine Info Nachricht.
func (s Logger) Info(f string, x ...interface{}) {
	s.requestlogger.Infof(f, x...)

}

// Logged eine Warnung.
func (s Logger) Warn(f string, x ...interface{}) {
	s.requestlogger.Warnf(f, x...)

}

// Logged einen Error.
func (s Logger) Error(f string, x ...interface{}) {
	s.requestlogger.Errorf(f, x...)
}

// Logged ein Fatal.
func (s Logger) Fatal(f string, x ...interface{}) {
	s.requestlogger.Fatalf(f, x...)
}

// Logged ein Debug.
func (s Logger) Debug(f string, x ...interface{}) {
	s.requestlogger.Debugf(f, x...)

}

//Initialisiert wie das Log aussieht.
func init() {
	//Setzt den output auf outpuLog.log und erweitert das bei jeder meldung
	file, err := os.OpenFile("outputLog.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	//setzt den Output Writer: Log in Konsole und Datei schrieben
	multiWriter := io.MultiWriter(file, os.Stdout)
	log.SetOutput(multiWriter)

	//nutzt JSON zum formatieren und nicht den Standard ASCII Formatierer
	f := new(prefixed.TextFormatter)
	f.ForceColors = true
	f.ForceFormatting = true
	f.FullTimestamp = true
	f.TimestampFormat = "2006-01-02 15:04:05"
	f.SpacePadding = 80
	log.SetFormatter(f)

	//setzt diese Ausgabe auf Infos vom Level Warnung oder höher
	logLevel := os.Getenv("MASP_LOG")
	switch logLevel {
	case "Debug":
		log.SetLevel(log.DebugLevel)
	case "Info":
		log.SetLevel(log.InfoLevel)
	case "Warn":
		log.SetLevel(log.WarnLevel)
	case "Error":
		log.SetLevel(log.ErrorLevel)
	default:
		log.SetLevel(log.InfoLevel)

	}

}
