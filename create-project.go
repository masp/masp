package masp

//erstellt ein neues Projekt und eine Projektdatei in der Datenbank
func createProject() {
	//ToDo Methode anlegen einer Projekt Tabelle mit allen wichtigen infos
	if m.GetSetting("project-name") == "" {
		m.SaveSetting("project-name", "Neues Projekt")
	}
	if m.GetSetting("project-description") == "" {
		m.SaveSetting("project-description", "Klicken Sie links auf das Zahnrad-Symbol um diese Beschreibung anzupassen.")
	}
	if m.GetSetting("project-links") == "" {
		m.SaveSetting("project-links", "{}")
	}
	_, _, version, _ := m.module.Info()
	m.SaveSetting("project-version", version.String())
}
