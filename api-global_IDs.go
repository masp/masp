package masp

import (
	"database/sql"
	"strconv"

	"github.com/gin-gonic/gin"
)

// GetNewGlobalID erstellt eine neue Global ID und gibt diese etwas zurück.
func (a API) GetNewGlobalID() int {
	log := m.Log("GlobalID")

	_, module, _, _ := a.module.Info()
	log.Debug("New global ID request for %s", module)
	var prefix int
	error := m.GetDatabase().QueryRow("SELECT prefix FROM base_module_IDs WHERE module = ?", module).Scan(&prefix)
	switch {
	case error == sql.ErrNoRows:
		log.Debug("Module has no id yet")
		result, error := m.GetDatabase().Exec("INSERT INTO base_module_IDs (module) VALUES (?)", module)
		if error != nil {
			log.Error("there was a tiny itsy bitsy error while generating a new ID: %s", error)
			return -1
		}
		prefix64, _ := result.LastInsertId()
		prefix = int(prefix64)
		log.Debug("The module id is %d", prefix)

	case error != nil:
		log.Error("this error tells me that something was not quite as expected, please note the following: %s", error)
		return -1
	}
	var maxGlobalID int
	prefixString := strconv.Itoa(prefix)
	error = m.GetDatabase().QueryRow("SELECT Max(global_ID) FROM base_global_IDs WHERE prefix = ?", prefix).Scan(&maxGlobalID)
	log.Debug("Max global id for module %s is %d", module, maxGlobalID)
	if error != nil {
		log.Debug("No global id for %s in database", module)
		prefixString += "1"
		globalID, _ := strconv.Atoi(prefixString)
		_, err := m.GetDatabase().Exec("INSERT INTO base_global_IDs (global_ID, prefix) VALUES (?, ?)", globalID, prefix)
		if err != nil {
			log.Error("Failed to write to db: %s", err)
		}
		log.Debug("Created new global id %d for %s", globalID, module)
		return globalID
	}

	currentGlobalID, _ := strconv.Atoi(strconv.Itoa(maxGlobalID)[1:])
	globalIDString := prefixString + strconv.Itoa(currentGlobalID+1)
	globalID, _ := strconv.Atoi(globalIDString)

	_, err := m.GetDatabase().Exec("INSERT INTO base_global_IDs (global_ID, prefix) VALUES (?, ?)", globalID, prefix)
	if err != nil {
		log.Error("Failed to write to db: %s", err)
	}

	log.Debug("Created new global id %d for %s", globalID, module)
	return globalID
}

// GetModuleByGlobalID gibt Module string via Global ID aus bei erfolg, sonst Fehler und empty String
func (a API) GetModuleByGlobalID(globalID int) string {
	log := m.Log("ModuleByglobalID")
	var prefix int
	error := m.GetDatabase().QueryRow("SELECT prefix FROM base_global_IDs WHERE global_ID = ?", globalID).Scan(&prefix)

	if error != nil {
		log.Error("Something is really really wrong, please panic now, issue when checking for prefix: %s", error)
		emptystring := ""
		return emptystring
	}

	var module string
	error = m.GetDatabase().QueryRow("SELECT module FROM base_module_IDs WHERE prefix = ?", prefix).Scan(&module)

	if error != nil {
		log.Error("Instant panic please, there is an issue when looking up the module via prefix: %s", error)
		return ""
	}

	return module
}

// GetModuleByPrefix gibt das Modul und true zurück, wenn der Prefix existiert, sonst "" und false
func (a API) GetModuleByPrefix(prefix int) (string, bool) {
	log := m.Log("ModuleByPrefix")
	var module string
	error := m.GetDatabase().QueryRow("SELECT module FROM base_module_IDs WHERE prefix = ?", prefix).Scan(&module)

	if error == sql.ErrNoRows {
		log.Error("No Row in SQL")
		return "", false
	}

	if error != nil && error != sql.ErrNoRows {
		log.Error("Instant panic please, there is an issue when looking up the module via prefix: %s", error)
		return "", false
	}

	return module, true
}

// GetModuleByPrefix gibt das Modul und true zurück, wenn der Prefix existiert, sonst "" und false
func (a API) GetPrefixByModule(module string) int {
	log := m.Log("PrefixByModule")
	var prefix int
	error := m.GetDatabase().QueryRow("SELECT prefix FROM base_module_IDs WHERE module = ?", module).Scan(&prefix)

	if error != nil {
		log.Error("Instant panic please, there is an issue when looking up the module via prefix: %s", error)
		return 0
	}

	return prefix
}

//GetinstanzbymoduleID gibt ein Modul-Object zurück und nil bei Fehler
func GetInstanceByModule(module string) Module {
	log := m.Log("ModuleObjectViaModule")

	for _, mod := range modules {
		_, name, _, _ := mod.Info()
		if module == name {
			return mod
		}
	}
	log.Error("No Module find via name")
	return nil
}

func moduleByID(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	module, isPrefix := m.GetModuleByPrefix(id)
	if isPrefix && module != "" {
		c.JSON(200, gin.H{
			"module": module,
		})
		return
	}
	module = m.GetModuleByGlobalID(id)
	if module != "" {
		c.JSON(200, gin.H{
			"module": module,
			"id":     id,
		})
	} else {
		c.Status(400)
	}
}
