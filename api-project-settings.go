package masp

import (
	"database/sql"
	"io/ioutil"

	"github.com/gin-gonic/gin"
)

//funktuioniert vom Prinziep her wie Get und Set
//man erhält informationen aus dem fronend und speihert es in der datenbank

//Route zum auslesen der aktuellen Einstellungen aus dem frontend
func getSetting(c *gin.Context) {
	name := c.Param("name")
	if name == "" {
		c.String(400, "need a name")
		return
	}

	c.String(200, m.GetSetting(name))
}

//GetSetting ermöglicht Aufruf von Außen. Für den Namen wird in der Datenbank der Wert nachgeguckt und zurück gegeben.
// Wenn es nicht existiert, wird ein leerer String abgegeben.
func (a API) GetSetting(name string) string {
	//lese aus der Datenbank
	row := m.GetDatabase().QueryRow("SELECT value FROM base_settings WHERE name = ?", name)
	var wert string
	l := m.Log("Get Setting")

	if err := row.Scan(&wert); err != nil {
		//name wird aus der aktuellen Zeile geladen
		if err != sql.ErrNoRows {
			//Fehlerbahandlung
			l.Error("Database Error: %s", err)
			return ""
		}
		//nichts gefunden
		return ""
	}
	return wert
}

// Damit die Funktion auch im Basismodul geladen werden kann aus dem Nutzermodul, ohne dass es eine Import-Loop gibt.
var RequireAdmin func(c *gin.Context) bool

//speichern der Einstellung von der Website in die Datenbank
func saveSetting(c *gin.Context) {
	if !RequireAdmin(c) {
		return
	}

	l := m.Log("Save Settings")

	name := c.Param("name")
	if name == "" {
		c.String(400, "need a name")
		return
	}

	var wert, err = ioutil.ReadAll(c.Request.Body)
	if err != nil {
		l.Debug("Body stream error: %s", err)
		return
	}

	//Schreibe in die Datenbank
	if err := m.SaveSetting(name, string(wert)); err != nil {
		c.String(500, err.Error())
	} else {
		c.String(200, string(wert))
	}
}

//SaveSetting speichert einen neuen Wert für den Namen in die Datenbank.
func (a API) SaveSetting(name, wert string) error {

	l := m.Log("Save Setting")

	_, err := m.GetDatabase().Exec("REPLACE INTO base_settings(name, value) VALUES (?, ?)", name, wert)
	if err != nil {
		// Fehler behandeln
		l.Error("Database Error: %s", err)
		return err
	}
	return nil
}
