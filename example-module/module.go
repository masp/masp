package example

import (
	"path"
	"runtime"

	"git.mo-mar.de/masp/masp"
	"github.com/coreos/go-semver/semver"
	"github.com/gin-gonic/gin"
)

// Module definiert das Modul
type Module struct{}

// M ist die Instanz des Moduls und muss immer so heißen, damit andere Module darauf zugreifen können
var M = Module{}

// m referenziert die API des Basismoduls und ist private, damit kein Modul sich als ein anderes ausgeben kann
var m = masp.RegisterModule(&M)

// Info gibt Informationen über das Modul aus (Name, ID, Version)
func (*Module) Info() (string, string, *semver.Version, string) {
	_, filename, _, _ := runtime.Caller(0)
	return "Example", "hello", semver.New("1.0.0"), path.Dir(filename)
}

// Routes richtet die vom Modul benötigten Routen ein
func (*Module) Routes(router *gin.RouterGroup) {
	// Beispiel: GIN Route mit URL-Parametern für Funktion erstellen
	// http://localhost:8080/api/hello/Alexa
	router.GET("/:world", wasauchimmer)
	router.POST("/:world", wasauchimmer)

	m.Log("bacon").Warn("Eine Wilde Horde %s apperad", "bacon")
}
