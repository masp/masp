const modules = [];

// Example 1.0.0
import helloComponent from "../example-module/frontend/module.vue"; modules.push(helloComponent);

// User Accounts 0.1.0
import usersComponent from "../../users/frontend/module.vue"; modules.push(usersComponent);

// Task List 0.1.0
import tasksComponent from "../../tasks/frontend/module.vue"; modules.push(tasksComponent);

export default modules;
