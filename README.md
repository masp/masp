# masp - make a simple project

Software zur Organisation kleiner Projekte anhand von interaktiven Aufgabenlisten.

![](https://get.mo-mar.de/screenshots/masp.png)

# Demo

➜ **[demo.projects.mo-mar.de](https://demo.projects.mo-mar.de)**  
**Username:** `demo@example.org`  
**Password:** `demo123`

# Installation
Voraussetzung: [Docker](https://docker.com/)

```bash
$ docker run -v "$PWD/data:/data" -p 8080:80 --name masp momar/masp
```

masp ist nun auf http://localhost:8080 erreichbar.
