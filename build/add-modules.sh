#!/bin/sh
set -eu

moduleinfo() {
	cat <<EOF > moduleinfo.go
package main
import (
	"fmt"
	"os"
	"path/filepath"
	module "$1"
)
func main() {
	name, id, version, path := module.M.Info()
	wd, _ := os.Getwd()
	relpath, _ := filepath.Rel(wd, path)
	fmt.Printf("%s\n%s\n%s\n%s\n", name, id, version, relpath)
}
EOF
	go run moduleinfo.go
}

# Backend
cat <<EOF > ../cmd/modules.go
package main

import (
EOF

# Frontend
cat <<EOF > ../frontend/modules.js
const modules = [];
EOF

for m in `cat modules.txt`; do
	echo "$m" | grep -vE '^\s*(#|$)' >/dev/null
	if [ $? -ne 0 ]; then
		continue;
	fi

	echo "Adding module $m..."
  go list "$m" >/dev/null 2>&1 || go get "$m"
  
	mi=`moduleinfo "$m"`
	name=`echo "$mi" | head -n-3 | tr '\n' ' ' | sed 's/ $//'`
	id=`echo "$mi" | tail -n3 | head -n1`
	version=`echo "$mi" | tail -n2 | head -n1`
	path=`echo "$mi" | tail -n1`
	echo "  $name $version ($id)"
	echo "  $path"

	# Backend
	cat <<EOF >> ../cmd/modules.go

	// $name $version
	_ "$m"
EOF

	# Frontend
	cat <<EOF >> ../frontend/modules.js

// $name $version
import ${id}Component from "$path/frontend/module.vue"; modules.push(${id}Component);
EOF
done

# Backend
cat <<EOF >>  ../cmd/modules.go

)
EOF

# Frontend
cat <<EOF >> ../frontend/modules.js

export default modules;
EOF
