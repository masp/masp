package masp

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

var (
	green        = string([]byte{27, 91, 57, 55, 59, 52, 50, 109})
	white        = string([]byte{27, 91, 57, 48, 59, 52, 55, 109})
	yellow       = string([]byte{27, 91, 57, 48, 59, 52, 51, 109})
	red          = string([]byte{27, 91, 57, 55, 59, 52, 49, 109})
	blue         = string([]byte{27, 91, 57, 55, 59, 52, 52, 109})
	magenta      = string([]byte{27, 91, 57, 55, 59, 52, 53, 109})
	cyan         = string([]byte{27, 91, 57, 55, 59, 52, 54, 109})
	reset        = string([]byte{27, 91, 48, 109})
	disableColor = false
	forceColor   = false
)

// Nutzung des Masp Loggers für Gin
func logGin(c *gin.Context) {
	if !gin.IsDebugging() {
		return
	}
	start := time.Now()
	path := c.Request.URL.Path
	c.Next()
	latency := time.Now().Sub(start)
	clientIP := c.ClientIP()
	method := c.Request.Method
	statusCode := c.Writer.Status()
	errorMessage := c.Errors.ByType(gin.ErrorTypePrivate).String()
	if errorMessage != "" {
		errorMessage = "\n" + errorMessage
	}

	statusColor := statusCodeColor(statusCode)
	mehtodColor := methodStringColor(method)
	m.Log("Gin").Debug("%s %3d %s| %13v | %15s |%s %-7s %s %s%s",
		statusColor, statusCode, reset,
		latency,
		clientIP,
		mehtodColor, method, reset,
		path,
		errorMessage)
}

func statusCodeColor(code int) string {
	switch {
	case code >= http.StatusOK && code < http.StatusMultipleChoices:
		return green
	case code >= http.StatusMultipleChoices && code < http.StatusBadRequest:
		return white
	case code >= http.StatusBadRequest && code < http.StatusInternalServerError:
		return yellow
	default:
		return red
	}
}

func methodStringColor(method string) string {
	switch method {
	case "GET":
		return blue
	case "POST":
		return cyan
	case "PUT":
		return yellow
	case "DELETE":
		return red
	case "PATCH":
		return green
	case "HEAD":
		return magenta
	case "OPTIONS":
		return white
	default:
		return reset
	}
}

func debugRoutePrint(httpMethod, absolutePath, handlerName string, nuHandlers int) {
	m.Log("Gin").Debug("%s%6s%s %-25s --> %s (%d handlers)",
		methodStringColor(httpMethod), httpMethod, reset,
		absolutePath,
		handlerName,
		nuHandlers)
}
