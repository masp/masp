package masp

import (
	"database/sql"
	"path"

	//Verwendung von SQL-Light
	_ "github.com/mattn/go-sqlite3"
	"github.com/pressly/goose"
)

var db = initDB("storage.db")

//initDB initialisiere DB sprache
func initDB(filepath string) *sql.DB {
	goose.SetDialect("sqlite3")
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		panic(err)
	}

	if db == nil {
		panic("db nil")
	}
	return db
}

//migrate initialisiert die DB für jedes Moduls
func migrate(db *sql.DB) error {
	modul_name, _, _, dir := m.module.Info()
	err := goose.Up(db, path.Join(dir, "./database"))

	if err != nil {
		m.Log("Migrate Database").Error("Creation of SQL database failed for: %s; Error is: %s", modul_name, err)

	}
	return err
}

//migrateModules erstellt Tabellen in der DB für jedes Modul
func migrateModules(db *sql.DB) {

	for _, mod := range modules {
		modul_name, moduleID, _, dirModule := mod.Info()
		goose.SetTableName("goose_" + moduleID)
		err := goose.Up(db, path.Join(dirModule, "./database"))

		if err != nil {
			m.Log("Migrate Database").Error("Creation of SQL database failed for: %s; Error is: %s", modul_name, err)
		}
	}
}

//GetDatabase gibt die Datenbank zurück
func (a API) GetDatabase() *sql.DB {
	return db
}
