package masp

// API enthält die API des Basismoduls und das Modul das diese API aufruft
type API struct {
	module Module
}

// API des Basismoduls wie in User Stories

// Translate übersetzt einen String in die Sprache des Nutzers aus einem GIN-Kontexts
// Achtung!: Diese funktion ist noch nicht implementiert
func (a API) Translate(s string) string {
	return "Hallo"
}
