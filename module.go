// Dies ist das Basismodul für masp.
// masp steht für make a simple project.
package masp

import (
	"path"
	"runtime"

	"github.com/coreos/go-semver/semver"
	"github.com/gin-gonic/gin"
)

// BaseModule definiert das Modul
type BaseModule struct{}

// m referenziert die API des Basismoduls und ist private, damit kein Modul sich als ein anderes ausgeben kann
var m = API{module: &BaseModule{}}

// Info gibt Informationen über das Modul aus (Name, ID, Version, Verzeichnis)
func (*BaseModule) Info() (string, string, *semver.Version, string) {
	_, filename, _, _ := runtime.Caller(0)
	return "Base", "base", semver.New("1.0.0"), path.Dir(filename)
}

// Routes richtet die vom Modul benötigten Routen ein
func (*BaseModule) Routes(router *gin.RouterGroup) {
	router.GET("/settings/:name", getSetting)
	router.PUT("/settings/:name", saveSetting)
	router.GET("/module-by-id/:id", moduleByID)
}
