package example

import "github.com/gin-gonic/gin"

type messageObject struct {
	Value string `json:"message"`
}

func wasauchimmer(c *gin.Context) {
	// Beispiel: Query-Parameter - Zusätzliche Daten zu einer Anfrage (z.B. Suchwort)
	// http://localhost:8080/api/hello/Alexa?wort=Moin&suffix=wie%20geht's%3F
	// In JavaScript enkodieren: encodeURIComponent("wie geht's?")
	w := c.Query("wort")
	if w == "" {
		w = "Hello"
	}

	m.Log("bacon").Warn("Eine Wilde Horde %s apperad", "bacon")

	// Beispiel: Header - Metadaten zu einer Anfrage (z.B. für Authentifizierung)
	stup := c.GetHeader("Masp-Is-Stupid")
	if stup != "" {
		stup = "You are stupid!"
	}

	// Beispiel: Body - Daten der Anfrage selbst (z.B. Objekt das in die Datenbank soll)
	msgo := messageObject{}
	if c.Request.Method == "POST" {
		c.BindJSON(&msgo)
	}

	// Sendet an den Client: "w world suffix\nYou are Stupid!\nMessage" (als Text!)
	c.String(200, w+" "+c.Param("world")+" "+c.Query("suffix")+"\n"+stup+"\n"+msgo.Value)
	// Sendet an den Client: { message: "Message" } (als Objekt!)
	//c.JSON(200, msgo) // Nur 1x senden pro Funktion → auskommentiert
}
