-- +goose Up
-- +goose NO TRANSACTION
-- SQL in this section is executed when the migration is applied.
CREATE TABLE IF NOT EXISTS base_settings
(
  name text NOT NULL PRIMARY KEY, 
  value text
);

CREATE TABLE IF NOT EXISTS base_module_IDs
(
  prefix INTEger PRIMARY KEY AUTOINCREMENT,
  module text NOT NULL
);

CREATE TABLE IF NOT EXISTS base_global_IDs
(
  global_ID INT NOT NULL PRIMARY KEY,
  prefix INT NOT NULL,
  FOREIGN KEY (prefix) REFERENCES base_module_IDs (prefix)
);
