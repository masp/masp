package masp

import (
	"bytes"
	"errors"
	"html/template"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	gomail "gopkg.in/gomail.v2"
)

// I Need:
// - Funktion zum Registrieren eines SMTP Servers / ändern - Done
// - Funktion um SMTP Daten abzufragen - Done
// - Routen, über die obige Funktionen erreichbar sind - Done
// - Eine API Funktion, über die Emails versendet werden können - Done
// - speichern meiner SMTP Daten -  Done, bis Datenbank implementiert, siehe TODO
// - Möglichkeit für Templates - Done
// - Mailinglisten annehmen - Done

// In dieser Datei ist geht es nur um den Mailversand aus MASP.
// Mails werden nur über SMTP versendet.

// mailingSettings : SMTP Einstellungen
type mailingSettings struct {
	Host     string
	Port     int
	User     string
	Password string
	Sender   string
}

// MailDisabled gibt true zurück, wenn keine SMTP Einstellungen definiert sind.
func (a API) MailDisabled() bool {
	s := getSMTPSettingsFromDatabase()
	return s.Host == "" || s.Password == "" || s.Port == 0 || s.Sender == "" || s.User == ""
}

// SendMail sendet eine Mail vom System aus. Die SMTP Daten werden dafür aus der Datenbank extrahiert.
// Die Mail geht an alle Nutzer in der Map `receiver`. Format `map[string]string{"mail@example.com":"Max Musterman",}`
// Für die E-Mail wird ein Template aus dem `mail-templates`-Ordner des aktuellen Moduls verwendet (z.B. `example.tpl`).
// Um Daten in das Template einzufügen schreibt man: `map[string]interface{}{"Subject":"Betreff",}`
// Auch der Betreff wird über die `templateData`-Map angegeben!
// Will der Entwickler Mails an mehrere Nutzer senden und die Namen in der Nachricht verwenden, muss er dies in seinem Code machen und in die Tamplate Data einbauen.
func (a API) SendMail(receivers map[string]string, template string, templateData map[string]interface{}) error {
	l := m.Log("Mail API")
	settings := getSMTPSettingsFromDatabase()
	l.Debug("Got SMTP Settings from Database: %v", settings)
	if settings.Host == "" || settings.Password == "" || settings.Port == 0 || settings.Sender == "" || settings.User == "" {
		l.Error("The SMTP settings are incomplete or don't exist. Please go to the Settings Menu in the webinterface and add correct SMTP settings")
		return errors.New("Mail disabled")
	}

	subject, body, err := processTemplate(template, templateData, a.module)
	if err != nil {
		l.Error("Failed to process Template: %s", err)
		return err
	}
	l.Debug("Dialing for SMTP connection...")
	d := gomail.NewDialer(settings.Host, settings.Port, settings.User, settings.Password)
	s, err := d.Dial()
	if err != nil {
		l.Error("Failure while dialing SMTP connection: %s", err)
		return err
	}
	l.Info("SMTP dial succesfull")

	var combinedErr []string

	m := gomail.NewMessage(gomail.SetCharset("UTF-8"))
	for adress, name := range receivers {
		m.SetHeader("From", settings.Sender)
		if name == "" {
			m.SetHeader("To", adress)
		} else {
			m.SetAddressHeader("To", adress, name)
		}
		m.SetHeader("Subject", subject)
		m.SetBody("text/plain", body)
		if err := gomail.Send(s, m); err != nil {
			l.Error("An Error accured while sending a Mail to %s (%s) with the subject %s. The error returned is: %s", name, adress, subject, err)
			combinedErr = append(combinedErr, err.Error())
		} else {
			l.Info("Successfull send mail to %s (%s) with subject %s", name, adress, subject)
		}
		m.Reset()
	}
	// Wenn die Länge nicht abgefragt wird, erstellt er immer einen neuen Error mit einem leeren string
	if len(combinedErr) > 0 {
		return errors.New(strings.Join(combinedErr, "\n"))
	}
	l.Debug("All Mails send succesfull.")
	return nil
}

//testMail sendet eine Mail an die angegebene Mail-Adresse
func testMail(receiver string) error {
	l := m.Log("Test Mail")
	l.Info("Sending Test Mail to %s", receiver)
	if receiver == "" {
		l.Error("No receiver specified!")
		return errors.New("No receiver specified for Test Mail")
	}
	err := m.SendMail(map[string]string{
		receiver: "",
	}, "test", map[string]interface{}{
		"Subject": "Masp Test Mail",
	})
	if err != nil {
		l.Error("An error occured while sending a Mail: %s", err)
	} else {
		l.Info("Test Mail was send succesful.")
	}
	return err
}

// processTemplate ist eine Hilfsfunktion. Nimmt den Namen des Templates und ein Data-Set und überführt dieses in ein Byte-Array, welches als Body für die Mail benötigt wird.
// Das Template muss im Ordner ./mail-templates/ liegen und TemplateName.tpl heißen. Templates sollten nach https://golang.org/pkg/text/template/ formatiert sein.
// In der ersten Zeile des Template sollte `Subject: {{ .Subject }}` schreiben. Dort können auch weitere Header hingeschrieben werden. Zwischen Header und den Body müssen zwei Zeilenumbrüche.
// Zurückgegeben wird: Betreff, Body, Error
func processTemplate(templateName string, templateData map[string]interface{}, module Module) (string, string, error) {
	l := m.Log("Template Processing")
	l.Info("Starting template processing ...")
	l.Debug("Template name %s", templateName)
	name, _, _, dir := module.Info()
	l.Debug("Mail Template for %s Module in this path: %s", name, dir)
	templatePath := filepath.Join(dir, "mail-templates", templateName+".tpl")
	if _, err := os.Stat(templatePath); os.IsNotExist(err) {
		l.Error("The requested Template %s is not contained in Module %s. Please create the template in this Path: %s", templateName, name, templatePath)
		return "", "", errors.New("This Template is not included in this Module (" + name + ").")
	}
	l.Info("Parsing Template in %s", templatePath)
	tmpl, err := template.ParseFiles(templatePath)
	if err != nil {
		l.Error("Failed to parse Template %s for Module %s: %s", templateName, name, err)
		return "", "", err
	}
	l.Debug("Parsing succesfull")
	l.Info("Executing Template....")
	buffer := new(bytes.Buffer)
	err = tmpl.Execute(buffer, templateData)
	if err != nil {
		l.Error("Failed to Execute template: %s", err)
		return "", "", errors.New("Failed to execute template")
	}
	tmplArr := strings.SplitN(buffer.String(), "\n", 2)
	l.Info("Templateprocessing complete!")
	return strings.TrimSpace(tmplArr[0]), strings.TrimSpace(tmplArr[1]), nil
}

// getSMTPSettingsFromDatabase ruft die Daten aus der Datenbank ab und gibt sie als MailingsSettings Objekt zurück
func getSMTPSettingsFromDatabase() mailingSettings {
	settings := mailingSettings{}
	settings.Host = os.Getenv("MASP_SMTP_HOST")
	settings.Port, _ = strconv.Atoi(os.Getenv("MASP_SMTP_PORT"))
	if settings.Port == 0 {
		settings.Port = 587
	}
	settings.User = os.Getenv("MASP_SMTP_USERNAME")
	settings.Password = os.Getenv("MASP_SMTP_PASSWORD")
	settings.Sender = os.Getenv("MASP_SMTP_FROM")
	return settings
}
