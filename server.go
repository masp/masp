package masp

import (
	"strings"

	"github.com/gin-gonic/gin"
	parcelServe "github.com/moqmar/parcel-serve"
	"github.com/pressly/goose"
)

//Start startet den Server und richtet alle Module ein - quasi die Main-Funktion von Masp
func Start() {
	r := gin.New()
	r.Use(logGin, gin.Recovery())
	gin.DebugPrintRouteFunc = debugRoutePrint

	// Initialisiere die Datenbanken für alle Module
	goose.SetLogger(GooseLogger{})
	migrate(db)
	migrateModules(db)
	createProject()

	// API-Routen (braucht das das Basismodul überhaupt?)
	m.module.Routes(r.Group("/api/" + "base"))

	// API-Routen aller anderen Module
	for _, m := range modules {
		_, mid, _, _ := m.Info()
		m.Routes(r.Group("/api/" + mid))
	}

	// Bleibt am Ende stehen - Frontend-Dateien an Parcel weiterleiten
	parcelServe.Serve("frontend", r, AssetNames(), MustAsset)

	r.Run()
}

// Nutzen unseren Logger, sodass die Logs einheitlich sind, Ist für alle anderen aber irrelevant
type GooseLogger struct{}

func (GooseLogger) Fatal(v ...interface{}) {
	m.Log("Goose").Fatal("", v...)
}
func (GooseLogger) Fatalf(format string, v ...interface{}) {
	m.Log("Goose").Fatal(strings.TrimSpace(format), v...)
}
func (GooseLogger) Print(v ...interface{})   { m.Log("Goose").Debug("", v...) }
func (GooseLogger) Println(v ...interface{}) { m.Log("Goose").Debug("", v...) }
func (GooseLogger) Printf(format string, v ...interface{}) {
	m.Log("Goose").Debug(strings.TrimSpace(format), v...)
}
